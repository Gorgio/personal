<?php

declare(strict_types=1);

namespace Tests\Blog\Tables;

use App\Blog\Entities\Post;
use App\Blog\Tables\PostTable;
use Framework\Database\NoRecordException;
use Pagerfanta\Pagerfanta;
use Tests\TestCases\DatabaseTestCase;

/**
 * @internal
 */
final class PostTableTest extends DatabaseTestCase
{
    /**
     * @var PostTable
     */
    private $postTable;

    protected function setUp(): void
    {
        $pdo = $this->getPDO();
        $this->seedDatabase($pdo);
        $this->postTable = new PostTable($pdo);
    }

    public function testFindWithCategory(): void
    {
        $post = $this->postTable->findWithCategory(1);
        static::assertInstanceOf(Post::class, $post);
    }

    public function testFindWithCategoryNonExistent(): void
    {
        static::expectException(NoRecordException::class);
        $this->postTable->findWithCategory(10000000);
    }

    public function testPublicPagination(): void
    {
        $pagination = $this->postTable->findPaginated(5, 2);
        static::assertInstanceOf(Pagerfanta::class, $pagination);
    }

    public function testPublicPaginationForCategory(): void
    {
        $pagination = $this->postTable->findPaginatedPublicForCategory(5, 2, 2);
        static::assertInstanceOf(Pagerfanta::class, $pagination);
    }

    /**
     * Find All with entity.
     */
    public function testFindAllWithEntity(): void
    {
        $posts = $this->postTable->findAll();
        static::assertCount(100, $posts);
        static::assertInstanceOf(Post::class, $posts[0]);
    }
}
