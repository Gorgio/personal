<?php

declare(strict_types=1);

namespace Tests\Framework\Middleware;

use App\Framework\Middleware\CSRFMiddleware;
use GuzzleHttp\Psr7\ServerRequest;
use ParagonIE\AntiCSRF\AntiCSRF;
use PHPUnit\Framework\TestCase;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @internal
 */
final class CSRFMiddlewareTest extends TestCase
{
    /**
     * @var CSRFMiddleware
     */
    private $middleware;
    private $session;

    protected function setUp(): void
    {
        $this->session = ['yolo' => 'yolo'];
        $this->middleware = $middleware = new CSRFMiddleware($this->session);
    }

    public function testLetGetRequestPass(): void
    {
        $handler = $this->getMockBuilder(RequestHandlerInterface::class)
            ->setMethods(['handle'])
            ->getMock()
        ;

        $handler->expects(static::once())
            ->method('handle')
        ;

        $request = (new ServerRequest('GET', 'http://localhost.com/demo', [], null, '1.1', [
            'REQUEST_URI' => '/demo',
        ]));

        $this->middleware->process($request, $handler);
    }

    public function testBlockPostRequestWithoutCSRF(): void
    {
        $handler = $this->getMockBuilder(RequestHandlerInterface::class)
            ->setMethods(['handle'])
            ->getMock()
        ;

        $handler->expects(static::never())
            ->method('handle')
        ;

        $request = (new ServerRequest('POST', 'http://localhost.com/demo', [], null, '1.1', [
            'REQUEST_URI' => '/demo',
        ]));

        $response = $this->middleware->process($request, $handler);
        static::assertSame(400, $response->getStatusCode());
    }

    public function testBlockPostRequestWithInvalidCSRFToken(): void
    {
        $handler = $this->getMockBuilder(RequestHandlerInterface::class)
            ->setMethods(['handle'])
            ->getMock()
        ;

        $handler->expects(static::never())
            ->method('handle')
        ;

        $request = (new ServerRequest('POST', 'http://localhost.com/demo', [], null, '1.1', [
            'REQUEST_URI' => '/demo',
        ]));

        $requestBody = $request->getParsedBody();
        $serverParams = $request->getServerParams();

        $csrft = new AntiCSRF($requestBody, $this->session, $serverParams);
        $tokenArray = $csrft->getTokenArray();
        $request = $request->withParsedBody([
            $csrft->getFormIndex() => $tokenArray[$csrft->getFormIndex()],
            $csrft->getFormToken() => 'InvalidCSRFToken',
        ]);

        $response = $this->middleware->process($request, $handler);
        static::assertSame(400, $response->getStatusCode());
    }

    public function testLetPostRequestWithValidCSRFTokenUnlocked(): void
    {
        $handler = $this->getMockBuilder(RequestHandlerInterface::class)
            ->setMethods(['handle'])
            ->getMock()
        ;

        $handler->expects(static::once())
            ->method('handle')
        ;

        $request = (new ServerRequest('POST', 'http://localhost.com/demo', [], null, '1.1', [
            'REQUEST_URI' => '/demo',
        ]));

        $requestBody = $request->getParsedBody();
        $serverParams = $request->getServerParams();

        $csrft = new AntiCSRF($requestBody, $this->session, $serverParams);
        $request = $request->withParsedBody($csrft->getTokenArray());
        $this->middleware->process($request, $handler);
    }

    public function testLetPostRequestWithValidCSRFTokenLockedToCorrectURI(): void
    {
        $handler = $this->getMockBuilder(RequestHandlerInterface::class)
            ->setMethods(['handle'])
            ->getMock()
        ;

        $handler->expects(static::once())
            ->method('handle')
        ;

        $request = (new ServerRequest('POST', 'http://localhost.com/demo', [], null, '1.1', [
            'REQUEST_URI' => '/demo',
        ]));

        $requestBody = $request->getParsedBody();
        $serverParams = $request->getServerParams();

        $csrft = new AntiCSRF($requestBody, $this->session, $serverParams);
        $request = $request->withParsedBody($csrft->getTokenArray('/demo'));
        $this->middleware->process($request, $handler);
    }

    public function testBlockPostRequestWithValidCSRFTokenLockedToIncorrectURI(): void
    {
        $handler = $this->getMockBuilder(RequestHandlerInterface::class)
            ->setMethods(['handle'])
            ->getMock()
        ;

        $handler->expects(static::never())
            ->method('handle')
        ;

        $request = (new ServerRequest('POST', 'http://localhost.com/demo', [], null, '1.1', [
            'REQUEST_URI' => '/demo',
        ]));

        $requestBody = $request->getParsedBody();
        $serverParams = $request->getServerParams();

        $csrft = new AntiCSRF($requestBody, $this->session, $serverParams);
        $request = $request->withParsedBody($csrft->getTokenArray('/notdemo'));
        $response = $this->middleware->process($request, $handler);
        static::assertSame(400, $response->getStatusCode());
    }
}
