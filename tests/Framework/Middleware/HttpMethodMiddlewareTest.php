<?php

declare(strict_types=1);

namespace Tests\Framework\Middleware;

use App\Framework\Middleware\HttpMethodMiddleware;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @internal
 */
final class HttpMethodMiddlewareTest extends TestCase
{
    /**
     * @var HttpMethodMiddleware
     */
    private $middleware;

    protected function setUp(): void
    {
        $this->middleware = new HttpMethodMiddleware();
    }

    public function testAddMethod(): void
    {
        $handler = $this->getMockBuilder(RequestHandlerInterface::class)
            ->setMethods(['handle'])
            ->getMock()
        ;

        $handler->expects(static::once())
            ->method('handle')
            ->with(static::callback(static function (ServerRequestInterface $request) {
                return 'DELETE' === $request->getMethod();
            }))
        ;

        $request = (new ServerRequest('POST', '/demo'))
            ->withParsedBody(['_METHOD' => 'DELETE'])
        ;

        $this->middleware->process($request, $handler);
    }
}
