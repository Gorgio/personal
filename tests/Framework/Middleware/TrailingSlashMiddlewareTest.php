<?php

declare(strict_types=1);

namespace Tests\Framework\Middleware;

use App\Framework\Middleware\TrailingSlashMiddleware;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * @internal
 */
final class TrailingSlashMiddlewareTest extends TestCase
{
    /**
     * @var TrailingSlashMiddleware
     */
    private $middleware;

    protected function setUp(): void
    {
        $this->middleware = new TrailingSlashMiddleware();
    }

    public function testRedirectTrailingSlash(): void
    {
        $handler = $this->getMockBuilder(RequestHandlerInterface::class)
            ->setMethods(['handle'])
            ->getMock()
        ;

        $request = (new ServerRequest('POST', '/demo/'));

        static::assertSame(301, $this->middleware->process($request, $handler)->getStatusCode());
    }
}
