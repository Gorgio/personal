<?php

declare(strict_types=1);

namespace Tests\Framework;

use Framework\Validator;
use Tests\TestCases\DatabaseTestCase;

/**
 * @internal
 */
final class ValidatorTest extends DatabaseTestCase
{
    /**
     * Required entries validator tests.
     */
    public function testRequiredIfKeyMissing(): void
    {
        $errors = $this->makeValidator([
            'name' => 'joe',
        ])
            ->required('name', 'content')
            ->getErrors()
        ;

        static::assertCount(1, $errors);

        static::assertSame('Field content is required', (string) $errors['content']);
    }

    public function testRequiredIfAllKeysArePresent(): void
    {
        $errors = $this->makeValidator([
            'name' => 'joe',
            'content' => 'content',
        ])
            ->required('name', 'content')
            ->getErrors()
        ;

        static::assertCount(0, $errors);
    }

    /**
     * Non empty entries validator tests.
     */
    public function testNotEmpty(): void
    {
        $errors = $this->makeValidator(['name' => ''])
            ->notEmpty('name')
            ->getErrors()
        ;

        static::assertCount(1, $errors);
    }

    /**
     * Length validator tests.
     */
    public function testLengthMinimumValidCase(): void
    {
        $errors = $this->makeValidator([
            'string' => '123456789',
        ])
            ->length('string', 3)
            ->getErrors()
        ;

        static::assertCount(0, $errors);
    }

    public function testLengthMinimumInvalidCase(): void
    {
        $errors = $this->makeValidator([
            'string' => '123456789',
        ])
            ->length('string', 15)
            ->getErrors()
        ;

        static::assertCount(1, $errors);
        static::assertSame('Field string must have more than 15 characters', (string) $errors['string']);
    }

    public function testLengthMaximumValidCase(): void
    {
        $errors = $this->makeValidator([
            'string' => '123456789',
        ])
            ->length('string', null, 20)
            ->getErrors()
        ;

        static::assertCount(0, $errors);
    }

    public function testLengthMaximumInvalidCase(): void
    {
        $errors = $this->makeValidator([
            'string' => '123456789',
        ])
            ->length('string', null, 2)
            ->getErrors()
        ;

        static::assertCount(1, $errors);
        static::assertSame('Field string must have less than 2 characters', (string) $errors['string']);
    }

    public function testLengthBetweenValidCase(): void
    {
        $errors = $this->makeValidator([
            'string' => '123456789',
        ])
            ->length('string', 5, 20)
            ->getErrors()
        ;

        static::assertCount(0, $errors);
    }

    public function testLengthBetweenInvalidCase(): void
    {
        $errors = $this->makeValidator([
            'string' => '123456789',
        ])
            ->length('string', 5, 8)
            ->getErrors()
        ;

        static::assertCount(1, $errors);
        static::assertSame('Field string must have between 5 and 8 characters', (string) $errors['string']);
    }

    public function testLengthOnUndefinedProperty(): void
    {
        static::assertCount(0, $this->makeValidator([])->length('string', 5)->getErrors());
    }

    /**
     * Datetime validator tests.
     */
    public function testDatetime(): void
    {
        static::assertCount(0, $this->makeValidator(['date' => '2010-12-26 11:54:32'])->datetime('date')->getErrors());
        static::assertCount(0, $this->makeValidator(['date' => '2010-12-26 00:00:00'])->datetime('date')->getErrors());
        static::assertCount(1, $this->makeValidator(['date' => '2010-20-26 11:54:32'])->datetime('date')->getErrors());
        static::assertCount(1, $this->makeValidator(['date' => '2010-20-26'])->datetime('date')->getErrors());
        static::assertCount(1, $this->makeValidator(['date' => '2015-02-29 11:54:32'])->datetime('date')->getErrors());
        static::assertCount(1, $this->makeValidator(['date' => '2015-02-29'])->datetime('date')->getErrors());
    }

    /**
     * Slug validator tests.
     */
    public function testSlugForValidSlug(): void
    {
        $errors = $this->makeValidator([
            'slug' => 'oy4oy-haj-3-er',
        ])
            ->slug('slug')
            ->getErrors()
        ;

        static::assertCount(0, $errors);
    }

    public function testSlugForInvalidSlug(): void
    {
        $errors = $this->makeValidator([
            'slug' => 'oy4Oy-haj-3-er',
            'slug2' => 'oyo_zrxgdx-s',
            'slug3' => 'azxrw--sfdc-eer',
            'slug4' => 'azrx-secg-',
        ])
            ->slug('slug')
            ->slug('slug2')
            ->slug('slug3')
            ->slug('slug4')
            ->getErrors()
        ;

        static::assertSame(['slug', 'slug2', 'slug3', 'slug4'], \array_keys($errors));
    }

    public function testExist(): void
    {
        $pdo = $this->getPDO();
        $pdo->exec('CREATE TABLE test (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(255)
        )');
        $pdo->exec('INSERT INTO test (name) VALUES  ("a1")');
        $pdo->exec('INSERT INTO test (name) VALUES  ("a2")');

        static::assertTrue($this->makeValidator(['category' => 1])->exists('category', 'test', $pdo)->isValid());
        static::assertFalse($this->makeValidator(['category' => 50])->exists('category', 'test', $pdo)->isValid());
    }

    public function testUnique(): void
    {
        $pdo = $this->getPDO();
        $pdo->exec('CREATE TABLE test (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(255)
        )');
        $pdo->exec('INSERT INTO test (name) VALUES  ("a1")');
        $pdo->exec('INSERT INTO test (name) VALUES  ("a2")');

        static::assertTrue($this->makeValidator(['name' => 'a5'])->unique('name', 'test', $pdo)->isValid());
        static::assertFalse($this->makeValidator(['name' => 'a1'])->unique('name', 'test', $pdo)->isValid());
        static::assertTrue($this->makeValidator(['name' => 'a5'])->unique('name', 'test', $pdo, 1)->isValid());
        static::assertFalse($this->makeValidator(['name' => 'a1'])->unique('name', 'test', $pdo, 2)->isValid());
    }

    private function makeValidator(array $params)
    {
        return new Validator($params);
    }
}
