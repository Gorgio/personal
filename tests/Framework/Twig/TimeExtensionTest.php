<?php

declare(strict_types=1);

namespace Tests\Framework\Twig;

use DateTime;
use Framework\Twig\TimeExtension;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class TimeExtensionTest extends TestCase
{
    /**
     * @var TimeExtension
     */
    private $timeExtension;

    protected function setUp(): void
    {
        $this->timeExtension = new TimeExtension();
    }

    public function testGetFilter(): void
    {
        $filters = $this->timeExtension->getFilters();
        static::assertInstanceOf(\Twig_SimpleFilter::class, $filters[0]);
    }

    public function testDateFormat(): void
    {
        $date = new DateTime();
        $format = 'd/m/Y H:i';
        $result = '<span class="timeago" datetime="' . $date->format(DateTime::ISO8601) . '">' .
            $date->format($format) . '</span>';
        static::assertSame($result, $this->timeExtension->ago($date));
    }
}
