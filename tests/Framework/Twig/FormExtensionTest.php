<?php

declare(strict_types=1);

namespace Tests\Framework\Twig;

use Framework\Twig\FormExtension;
use PHPUnit\Framework\TestCase;
use Twig_SimpleFunction;

/**
 * @internal
 */
final class FormExtensionTest extends TestCase
{
    /**
     * @var FormExtension
     */
    private $formExtension;

    protected function setUp(): void
    {
        $this->formExtension = new FormExtension();
    }

    public function testGetFilter(): void
    {
        $func = $this->formExtension->getFunctions();
        static::assertInstanceOf(Twig_SimpleFunction::class, $func[0]);
    }

    public function testInput(): void
    {
        $html = $this->formExtension->field([], 'name', 'demo', 'Name');
        self::assertSimilar(
            '<div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" value="demo">
            </div>',
            $html
        );
    }

    public function testTextarea(): void
    {
        $html = $this->formExtension->field([], 'name', 'demo', 'Name', ['type' => 'textarea']);
        self::assertSimilar(
            '<div class="form-group">
                <label for="name">Name</label>
                <textarea class="form-control" name="name" id="name">demo</textarea>
            </div>',
            $html
        );
    }

    public function testInputWithErrors(): void
    {
        $context = ['errors' => ['name' => 'error']];
        $html = $this->formExtension->field($context, 'name', 'demo', 'Name');
        self::assertSimilar(
            '<div class="form-group has-danger">
                <label for="name">Name</label>
                <input type="text" class="form-control form-control-danger" name="name" id="name" value="demo">
                <small class="form-text text-muted">error</small>
            </div>',
            $html
        );
    }

    public function testSelect(): void
    {
        $html = $this->formExtension->field(
            [],
            'name',
            '2',
            'Select Test',
            ['options' => [1 => 'Demo', 2 => 'demo2']]
        );
        self::assertSimilar('<div class="form-group">
                <label for="name">Select Test</label>
                <select class="form-control" name="name" id="name">
                    <option value="1">Demo</option>
                    <option value="2" selected>demo2</option>
                </select>
            </div>', $html);
    }

    private static function trim(string $string): string
    {
        $lines = \explode(\PHP_EOL, $string);
        $lines = \array_map('trim', $lines);

        return \implode('', $lines);
    }

    private static function assertSimilar(string $expected, string $actual): void
    {
        static::assertSame(self::trim($expected), self::trim($actual));
    }
}
