<?php

declare(strict_types=1);

namespace Tests\Framework\Twig;

use Framework\Twig\TextExtension;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class TextExtensionTest extends TestCase
{
    /**
     * @var TextExtension
     */
    private $textExtension;

    protected function setUp(): void
    {
        $this->textExtension = new TextExtension();
    }

    public function testGetFilter(): void
    {
        $filters = $this->textExtension->getFilters();
        static::assertInstanceOf(\Twig_SimpleFilter::class, $filters[0]);
    }

    public function testExcerptWithShortText(): void
    {
        $text = 'Hello';
        static::assertSame($text, $this->textExtension->excerpt($text, 10));
    }

    public function testExcerptWithLongTextTruncated(): void
    {
        $text = 'Hello Wonderful World';
        static::assertSame('Hello Wonderful...', $this->textExtension->excerpt($text, 18));
    }
}
