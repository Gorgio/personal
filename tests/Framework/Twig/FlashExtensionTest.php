<?php

declare(strict_types=1);

namespace Tests\Framework\Twig;

use App\Framework\Twig\FlashExtension;
use Framework\Sessions\FlashMessageService;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class FlashExtensionTest extends TestCase
{
    /**
     * @var FlashExtension
     */
    private $flashExtension;
    private $flashMessageService;

    protected function setUp(): void
    {
        $this->flashMessageService = $this->prophesize(FlashMessageService::class);

        $this->flashExtension = new FlashExtension($this->flashMessageService->reveal());
    }

    public function testGetFlash(): void
    {
        $this->flashMessageService->get(FlashMessageService::SUCCESS_KEY)->willReturn('success');

        $message = $this->flashExtension->getFlash(FlashMessageService::SUCCESS_KEY);
        static::assertSame('success', $message);
    }

    public function testGetFunctions(): void
    {
        $functions = $this->flashExtension->getFunctions();
        static::assertInstanceOf(\Twig_SimpleFunction::class, $functions[0]);
    }
}
