<?php

declare(strict_types=1);

namespace Tests\Framework\Sessions;

use Framework\Sessions\ArraySessions;
use Framework\Sessions\FlashMessageService;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 */
final class FlashMessageServiceTest extends TestCase
{
    /**
     * @var ArraySessions
     */
    private $session;

    /**
     * @var FlashMessageService
     */
    private $flashService;

    protected function setUp(): void
    {
        $this->session = new ArraySessions();
        $this->flashService = new FlashMessageService($this->session);
    }

    public function testDeleteFlashAfterHavingBeenSeen(): void
    {
        $this->flashService->success('Bravo');
        static::assertSame('Bravo', $this->flashService->get(FlashMessageService::SUCCESS_KEY));
        static::assertNull($this->session->get(FlashMessageService::SESSION_KEY));
        static::assertSame('Bravo', $this->flashService->get(FlashMessageService::SUCCESS_KEY));
        static::assertSame('Bravo', $this->flashService->get(FlashMessageService::SUCCESS_KEY));
    }
}
