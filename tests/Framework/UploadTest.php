<?php

declare(strict_types=1);

namespace Tests\Framework;

use Framework\Upload;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UploadedFileInterface;

/**
 * @internal
 */
final class UploadTest extends TestCase
{
    /**
     * @var Upload
     */
    private $upload;

    protected function setUp(): void
    {
        $this->upload = new Upload('tests');
    }

    protected function tearDown(): void
    {
        if (\file_exists('tests/demo.jpg')) {
            \unlink('tests/demo.jpg');
        }
    }

    public function testUpload(): void
    {
        $uploadedFile = $this->getMockBuilder(UploadedFileInterface::class)->getMock();

        $uploadedFile->expects(static::any())
            ->method('getClientFileName')
            ->willReturn('demo.jpg')
        ;

        $uploadedFile->expects(static::once())
            ->method('moveTo')
            ->with(static::equalTo('tests/demo.jpg'))
        ;

        static::assertSame('demo.jpg', $this->upload->upload($uploadedFile));
    }

    public function testUploadWithExistingFile(): void
    {
        $uploadedFile = $this->getMockBuilder(UploadedFileInterface::class)->getMock();

        $uploadedFile->expects(static::any())
            ->method('getClientFileName')
            ->willReturn('demo.jpg')
        ;

        \touch('tests/demo.jpg');

        $uploadedFile->expects(static::once())
            ->method('moveTo')
            ->with(static::equalTo('tests/demo_copy.jpg'))
        ;

        static::assertSame('demo_copy.jpg', $this->upload->upload($uploadedFile));
        \unlink('tests/demo.jpg');
    }
}
