<?php

declare(strict_types=1);

namespace Tests\Framework\Database;

use Framework\Database\NoRecordException;
use Framework\Database\Table;
use Pagerfanta\Pagerfanta;
use PDO;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

/**
 * @internal
 */
final class TableTest extends TestCase
{
    /**
     * @var Table
     */
    private $table;

    /**
     * @var PDO
     */
    private $pdo;

    protected function setUp(): void
    {
        $pdo = new PDO('sqlite::memory:', null, null, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ]);

        $pdo->exec('CREATE TABLE test (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(255)
        )');

        $this->pdo = $pdo;
        $this->table = new class($pdo) extends Table {
            protected const TABLE_NAME = 'test';
        };
    }

    // Insert method test.
    public function testInsert(): void
    {
        $status = $this->table->insert(['name' => 'demo']);
        static::assertTrue($status);
        $query = $this->table->getPdo()->query('SELECT name FROM test WHERE id = ?');
        $query->execute([$this->table->getPdo()->lastInsertId()]);
        $result = $query->fetch();
        static::assertSame('demo', $result->name);
    }

    public function testUpdate(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');
        $status = $this->table->update(1, ['name' => 'demo']);
        static::assertTrue($status);
        $statement = $this->table->getPdo()->query('SELECT name FROM test WHERE id = 1');
        $result = $statement->fetch();
        static::assertSame('demo', $result->name);
    }

    public function testDelete(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a3")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a4")');
        $status = $this->table->delete(2);
        static::assertTrue($status);

        $statement = $this->table->getPdo()->query('SELECT id FROM test');
        $result = $statement->fetchAll();
        static::assertCount(3, $result);
        static::assertSame('1', $result[0]->id);
        static::assertSame('3', $result[1]->id);
        static::assertSame('4', $result[2]->id);
    }

    public function testExist(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');
        static::assertTrue($this->table->exists(1));
        static::assertTrue($this->table->exists(2));
        static::assertFalse($this->table->exists(256456));
    }
    // Find method tests.
    public function testFind(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');

        $test = $this->table->find(1);
        static::assertInstanceOf(\stdClass::class, $test);
        static::assertSame('a1', $test->name);
    }

    public function testFindNonExistent(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');

        static::expectException(NoRecordException::class);
        $test = $this->table->find(5);
    }

    public function testFindBy(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');

        $category = $this->table->findBy('name', 'a1');
        static::assertInstanceOf(\stdClass::class, $category);
        static::assertSame('1', $category->id);
    }

    public function testFindByNonExistent(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');

        static::expectException(NoRecordException::class);
        $test = $this->table->findBy('name', 'a4');
    }

    public function testFindList(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a3")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a4")');
        $names = $this->table->findList();
        static::assertCount(4, $names);
        static::assertSame(['1' => 'a1', '2' => 'a2', '3' => 'a3', '4' => 'a4'], $names);
    }

    public function testFindAll(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a3")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a4")');
        $names = $this->table->findAll();
        static::assertCount(4, $names);
        static::assertInstanceOf(\stdClass::class, $names[0]);
        static::assertSame('a1', $names[0]->name);
        static::assertSame('a2', $names[1]->name);
    }

    // Test pagination.
    public function testPagination(): void
    {
        $this->seedLargeTestDatabase();
        $pagination = $this->table->findPaginated(4, 1);
        static::assertInstanceOf(Pagerfanta::class, $pagination);
    }

    // Test count.
    public function testCount(): void
    {
        $this->seedLargeTestDatabase();
        static::assertSame(12, $this->table->count());
    }

    // Test getters.
    public function testGetters(): void
    {
        static::assertInstanceOf(PDO::class, $this->table->getPdo());
        static::assertSame('test', $this->table->getTable());
        static::assertNull($this->table->getEntity());
    }

    public function testGetEntityNotNull(): void
    {
        $reflection = new ReflectionClass($this->table);
        $property = $reflection->getProperty('entity');
        $property->setAccessible(true);
        $property->setValue($this->table, 'DemoEntity');

        static::assertSame('DemoEntity', $this->table->getEntity());
    }

    private function seedLargeTestDatabase(): void
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a2")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a3")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a4")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a5")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a6")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a7")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a8")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a9")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a10")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a11")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES  ("a12")');
    }
}
