<?php

declare(strict_types=1);

namespace Framework\Renderer;

interface RendererInterface
{
    /**
     * Add paths to load view.
     *
     * @param string      $namespace
     * @param null|string $path
     */
    public function addPath(string $namespace, ?string $path = null): void;

    /**
     * Renders a view,
     * Paths can be provided with a namespace added with the addPath function.
     *
     * @param string $view
     * @param array  $params
     *
     * @return string
     */
    public function render(string $view, array $params = []): string;

    /**
     * Add a global variable to all views.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function addGlobal(string $key, $value): void;
}
