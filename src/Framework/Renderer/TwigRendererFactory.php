<?php

declare(strict_types=1);

namespace Framework\Renderer;

use App\Framework\Twig\AutoRouteGeneratorExtension;
use App\Framework\Twig\FlashExtension;
use Framework\Twig\CSRFFormExtension;
use Framework\Twig\FormExtension;
use Framework\Twig\PagerfantaExtension;
use Framework\Twig\TextExtension;
use Framework\Twig\TimeExtension;
use Psr\Container\ContainerInterface;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

class TwigRendererFactory
{
    public function __invoke(ContainerInterface $container): TwigRenderer
    {
        $cacheDir = 'tmp/views';

        $debug = $container->get('env') !== 'production';
        if ($debug) {
            $cacheDir = false;
        }

        $viewPath = $container->get('views.path');
        $loader = new FilesystemLoader($viewPath);
        $twig = new Environment(
            $loader,
            [
                'debug' => $debug,
                'cache' => $cacheDir,
                'auto_reload' => $debug,
            ]
        );
        // Debug extension!
        $twig->addExtension(new DebugExtension());

        $twig->addExtension($container->get(CSRFFormExtension::class));
        $twig->addExtension($container->get(FlashExtension::class));
        $twig->addExtension($container->get(PagerfantaExtension::class));
        $twig->addExtension($container->get(AutoRouteGeneratorExtension::class));
        $twig->addExtension(new FormExtension());
        $twig->addExtension(new TextExtension());
        $twig->addExtension(new TimeExtension());

        return new TwigRenderer($twig);
    }
}
