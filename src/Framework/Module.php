<?php

declare(strict_types=1);

namespace Framework;

class Module
{
    public const DEFINITIONS = null;

    public const MIGRATIONS = null;

    public const SEEDS = null;
}
