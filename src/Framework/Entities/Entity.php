<?php

declare(strict_types=1);

namespace Framework\Entities;

class Entity
{
    /**
     * @param string $property
     * @param mixed  $value
     */
    public function __set(string $property, $value): void
    {
        $setterMethod = 'set' . \str_replace('_', '', \ucwords($property, '_'));
        if (\method_exists($this, $setterMethod)) {
            $this->{$setterMethod}($value);
        }
    }
}
