<?php

declare(strict_types=1);

namespace Framework;

use Psr\Http\Message\UploadedFileInterface;

class Upload
{
    protected $path;

    protected $formats;

    public function __construct(?string $path = null)
    {
        if (!\is_null($path)) {
            $this->path = $path;
        }
    }

    public function upload(UploadedFileInterface $file): string
    {
        $targetPath = $this->addSuffix($this->path . \DIRECTORY_SEPARATOR . $file->getClientFilename());
        $file->moveTo($targetPath);

        return \pathinfo($targetPath)['basename'];
    }

    private function addSuffix(string $path): string
    {
        if (\file_exists($path)) {
            $info = \pathinfo($path);
            $path = $info['dirname'] . \DIRECTORY_SEPARATOR . $info['filename'] . '_copy.' . $info['extension'];

            return $this->addSuffix($path);
        }

        return $path;
    }
}
