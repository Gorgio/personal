<?php

declare(strict_types=1);

namespace Framework\Validator;

class ValidationError
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $rule;

    /**
     * @var string[]
     */
    private $messages = [
        'required' => 'Field %s is required',
        'empty' => 'Field %s can\'t be empty',
        'exist' => 'Field %s doesn\'t exist in table %s',
        'slug' => 'Field %s isn\'t a valid slug',
        'length-min' => 'Field %s must have more than %d characters',
        'length-max' => 'Field %s must have less than %d characters',
        'length-between' => 'Field %s must have between %d and %d characters',
        'datetime' => 'Field %s must be a valid date (%s)',
        'unique' => 'Field %s must be unique',
    ];

    /**
     * @var array
     */
    private $attributes;

    /**
     * ValidationError constructor.
     *
     * @param string $key
     * @param string $rule
     * @param array  $attributes
     */
    public function __construct(string $key, string $rule, array $attributes = [])
    {
        $this->key = $key;
        $this->rule = $rule;
        $this->attributes = $attributes;
    }

    public function __toString(): string
    {
        $params = \array_merge([$this->messages[$this->rule], $this->key], $this->attributes);

        return (string) \call_user_func_array('sprintf', $params);
    }
}
