<?php

declare(strict_types=1);

namespace Framework;

use DI\ContainerBuilder;
use Exception;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class App implements RequestHandlerInterface
{
    /**
     * @var string
     */
    private $env;

    /**
     * Module list.
     *
     * @var array
     */
    private $modules = [];

    /**
     * @var ?ContainerInterface
     */
    private $container;

    /**
     * Container definition directory.
     *
     * @var string
     */
    private $definitionDir;

    /**
     * @var string[]
     */
    private $middleware = [];

    /**
     * @var int
     */
    private $index = 0;

    public function __construct(string $definitionDir, string $env = 'production')
    {
        $this->definitionDir = $definitionDir;
        $this->env = $env;
    }

    /**
     * Add a Module to the application stack.
     *
     * @param string $module
     *
     * @return App
     */
    public function addModule(string $module): self
    {
        $this->modules[] = $module;

        return $this;
    }

    /**
     * Add a Middleware to the application stack.
     *
     * @param string $middleware
     *
     * @return App
     */
    public function pipe(string $middleware): self
    {
        $this->middleware[] = $middleware;

        return $this;
    }

    /**
     * Run the application stack and return an HTTP response.
     *
     * @param ServerRequestInterface $request
     *
     * @throws Exception
     *
     * @return ResponseInterface
     */
    public function run(ServerRequestInterface $request): ResponseInterface
    {
        $this->buildContainerWithRequest($request);

        foreach ($this->modules as $module) {
            $this->getContainer()->get($module);
        }

        return $this->handle($request);
    }

    /**
     * Handles a request and produces a response.
     *
     * May call other collaborating code to generate the response.
     *
     * @param ServerRequestInterface $request
     *
     * @throws Exception
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $middleware = $this->getMiddleware();

        if (\is_null($middleware)) {
            throw new \Exception('This request hasn\'t been intercept by any middleware.');
        }

        return $middleware->process($request, $this);
    }

    /**
     * @throws Exception
     *
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        if (\is_null($this->container)) {
            $builder = $this->getBuilder();
            if ($this->env === 'production') {
                $builder->enableCompilation('tmp');
                $builder->writeProxiesToFile(true, 'tmp/proxies');
            }
            $this->container = $builder->build();
        }

        return $this->container;
    }

    private function buildContainerWithRequest(ServerRequestInterface $request): void
    {
        $builder = $this->getBuilder();
        $builder->addDefinitions([ServerRequestInterface::class => $request]);
        if ($this->env === 'production') {
            $builder->enableCompilation('tmp');
            $builder->writeProxiesToFile(true, 'tmp/proxies');
        }
        $this->container = $builder->build();
    }

    private function getBuilder(): ContainerBuilder
    {
        $builder = new ContainerBuilder();
        $builder->addDefinitions($this->definitionDir . 'config.php');

        foreach ($this->modules as $module) {
            if (!\is_null($module::DEFINITIONS)) {
                $builder->addDefinitions($module::DEFINITIONS);
            }
        }
        $dbConfig = $this->env;
        if (\PHP_SAPI === 'cli') {
            $dbConfig = 'test';
        }
        $builder->addDefinitions($this->definitionDir . 'config-db-' . $dbConfig . '.php');

        return $builder;
    }

    /**
     * @return null|MiddlewareInterface
     */
    private function getMiddleware(): ?MiddlewareInterface
    {
        $middleware = $this->container->get($this->middleware[$this->index]);
        ++$this->index;

        return $middleware;
    }
}
