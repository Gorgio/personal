<?php

declare(strict_types=1);

namespace Framework\Database;

use Pagerfanta\Pagerfanta;
use PDO;

class Table
{
    /**
     * Name of the table in the database.
     *
     * @var string
     */
    protected const TABLE_NAME = self::TABLE_NAME;

    /**
     * Entity to utilize.
     *
     * @var ?class-string
     */
    protected $entity = null;
    /**
     * @var PDO
     */
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Paginates entities.
     *
     * @param int $perPage
     * @param int $currentPage
     *
     * @return Pagerfanta
     */
    public function findPaginated(int $perPage, int $currentPage): Pagerfanta
    {
        $query = new PaginatedQuery(
            $this->pdo,
            $this->paginationQuery(),
            'SELECT COUNT(id) FROM ' . static::TABLE_NAME,
            $this->entity
        );

        return (new Pagerfanta($query))
            ->setMaxPerPage($perPage)
            ->setCurrentPage($currentPage)
        ;
    }

    /**
     * Inserts an item entry in the database.
     *
     * @param array $params
     *
     * @return bool
     */
    public function insert(array $params): bool
    {
        $fields = \array_keys($params);
        $values = \implode(
            ', ',
            \array_map(
                static function (string $field): string {
                    return ":${field}";
                },
                $fields
            )
        );
        $fields = \implode(', ', $fields);
        $query = $this->pdo->prepare(
            'INSERT INTO ' . static::TABLE_NAME . " ({$fields}) VALUES ({$values})"
        );

        return $query->execute($params);
    }

    /**
     * Updates an item entry in the database.
     *
     * @param int   $id
     * @param array $params
     *
     * @return bool
     */
    public function update(int $id, array $params): bool
    {
        $fieldsQuery = $this->buildFieldsQuery($params);
        $params['id'] = $id;
        $query = $this->pdo->prepare('UPDATE ' . static::TABLE_NAME . " SET {$fieldsQuery} WHERE id = :id");

        return $query->execute($params);
    }

    /**
     * Deletes an item entry in the database.
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        $query = $this->pdo->prepare('DELETE FROM ' . static::TABLE_NAME . ' WHERE id = :id');
        $query->bindParam('id', $id, PDO::PARAM_INT);

        return $query->execute();
    }

    /**
     * Verifies if an entry exists.
     *
     * @param int $id
     *
     * @return bool
     */
    public function exists(int $id): bool
    {
        $query = $this->pdo->prepare('SELECT id FROM ' . static::TABLE_NAME . ' WHERE id = :id');
        $query->bindParam('id', $id, PDO::PARAM_INT);
        $query->execute();

        return $query->fetchColumn() !== false;
    }

    /**
     * Retrieves entity from ID.
     *
     * @param int $id
     *
     * @throws NoRecordException
     *
     * @return null|mixed
     */
    public function find(int $id)
    {
        return $this->fetchOrFail('SELECT * FROM ' . static::TABLE_NAME . ' WHERE id = :id', ['id' => $id]);
    }

    /**
     * Retrieve one line with help of one column.
     *
     * @param string $field
     * @param mixed  $value
     *
     * @throws NoRecordException
     *
     * @return null|mixed
     */
    public function findBy(string $field, $value)
    {
        return $this->fetchOrFail(
            'SELECT * FROM ' . static::TABLE_NAME . " WHERE ${field} = :value",
            ['value' => $value]
        );
    }

    /**
     * Retrieves a key => value list of our entries.
     */
    public function findList(): array
    {
        $results = $this->pdo
            ->query('SELECT id, name FROM ' . static::TABLE_NAME)
            ->fetchAll(PDO::FETCH_NUM)
        ;
        $list = [];
        foreach ($results as $result) {
            $list[$result[0]] = $result[1];
        }

        return $list;
    }

    /**
     * Retrieves all entries.
     *
     * @return array
     */
    public function findAll(): array
    {
        $query = $this->pdo->query('SELECT * FROM ' . static::TABLE_NAME);
        $query->setFetchMode(PDO::FETCH_OBJ);
        if ($this->entity) {
            $query->setFetchMode(PDO::FETCH_CLASS, $this->entity, []);
        }

        return $query->fetchAll();
    }

    /**
     * Count rows in a table.
     *
     * @return int
     */
    public function count(): int
    {
        return (int) $this->fetchColumn('SELECT COUNT(id) FROM ' . static::TABLE_NAME);
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return static::TABLE_NAME;
    }

    /**
     * @return string
     */
    public function getEntity(): ?string
    {
        return $this->entity;
    }

    /**
     * @return PDO
     */
    public function getPdo(): PDO
    {
        return $this->pdo;
    }

    protected function paginationQuery(): string
    {
        return 'SELECT * FROM ' . static::TABLE_NAME;
    }

    /**
     * Execute a query with named parameters and fetch the first line.
     *
     * @param string $query
     * @param array  $param
     *
     * @throws NoRecordException
     *
     * @return mixed
     */
    protected function fetchOrFail(string $query, array $param)
    {
        $query = $this->pdo->prepare($query);
        foreach ($param as $key => $value) {
            $query->bindParam(":${key}", $value);
        }
        $query->execute();
        if ($this->entity) {
            $query->setFetchMode(PDO::FETCH_CLASS, $this->entity, []);
        }
        $record = $query->fetch();
        if ($record === false) {
            throw new NoRecordException();
        }

        return $record;
    }

    /**
     * Retrieves first column of a query.
     *
     * @param string $query
     * @param array  $params
     *
     * @return mixed
     */
    protected function fetchColumn(string $query, array $params = [])
    {
        $query = $this->pdo->prepare($query);
        foreach ($params as $key => $value) {
            $query->bindParam(":${key}", $value);
        }
        $query->execute();
        if ($this->entity) {
            $query->setFetchMode(PDO::FETCH_CLASS, $this->entity, []);
        }

        return $query->fetchColumn();
    }

    /**
     * Build UPDATE SET query.
     *
     * @param array $params
     *
     * @return string
     */
    private function buildFieldsQuery(array $params): string
    {
        return \implode(
            ', ',
            \array_map(
                static function (string $field): string {
                    return "${field} = :${field}";
                },
                \array_keys($params)
            )
        );
    }
}
