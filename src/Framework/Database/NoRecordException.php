<?php

declare(strict_types=1);

namespace Framework\Database;

use Exception;

class NoRecordException extends Exception
{
}
