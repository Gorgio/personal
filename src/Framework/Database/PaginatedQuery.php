<?php

declare(strict_types=1);

namespace Framework\Database;

use Pagerfanta\Adapter\AdapterInterface;
use PDO;

class PaginatedQuery implements AdapterInterface
{
    /**
     * @var string
     */
    private $countQuery;
    /**
     * @var \PDO
     */
    private $pdo;
    /**
     * @var string
     */
    private $query;
    /**
     * @var null|class-string
     */
    private $entityClass;
    /**
     * @var array
     */
    private $params;

    /**
     * PaginatedQuery constructor.
     *
     * @param PDO               $pdo
     * @param string            $query       SQL query which selects X results
     * @param string            $countQuery  SQL query which counts the total number of results
     * @param null|class-string $entityClass
     * @param array             $params
     */
    public function __construct(
        PDO $pdo,
        string $query,
        string $countQuery,
        ?string $entityClass = null,
        array $params = []
    ) {
        $this->countQuery = $countQuery;
        $this->pdo = $pdo;
        $this->query = $query;
        $this->entityClass = $entityClass;
        $this->params = $params;
    }

    /**
     * Returns the number of results.
     *
     * @return int the number of results
     */
    public function getNbResults(): int
    {
        if (!empty($this->params)) {
            $query = $this->pdo->prepare($this->countQuery);
            foreach ($this->params as $key => $param) {
                $query->bindParam($key, $param);
            }
            $query->execute();

            return (int) $query->fetchColumn();
        }

        return (int) $this->pdo->query($this->countQuery)->fetchColumn();
    }

    /**
     * Returns an slice of the results.
     *
     * @param int $offset the offset
     * @param int $length the length
     *
     * @return array the slice
     */
    public function getSlice($offset, $length): array
    {
        $query = $this->pdo->prepare($this->query . ' LIMIT :length OFFSET :offset');
        foreach ($this->params as $key => $param) {
            $query->bindParam($key, $param);
        }
        $query->bindParam('length', $length, PDO::PARAM_INT);
        $query->bindParam('offset', $offset, PDO::PARAM_INT);
        if (!\is_null($this->entityClass)) {
            $query->setFetchMode(PDO::FETCH_CLASS, $this->entityClass, []);
        }
        $query->execute();

        return $query->fetchAll();
    }
}
