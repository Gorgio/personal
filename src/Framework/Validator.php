<?php

declare(strict_types=1);

namespace Framework;

use DateTime;
use Framework\Validator\ValidationError;
use PDO;

class Validator
{
    /**
     * @var array
     */
    private $params;

    /**
     * @var ValidationError[]
     */
    private $errors = [];

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Check if keys are present in array.
     *
     * @param string ...$keys
     *
     * @return Validator
     */
    public function required(string ...$keys): self
    {
        foreach ($keys as $key) {
            $value = $this->getValue($key);
            if (\is_null($value)) {
                $this->addError($key, 'required');
            }
        }

        return $this;
    }

    /**
     * Check if entries aren't empty.
     *
     * @param string ...$keys
     *
     * @return $this
     */
    public function notEmpty(string ...$keys)
    {
        foreach ($keys as $key) {
            $value = $this->getValue($key);
            if (\is_null($value) || empty($value)) {
                $this->addError($key, 'empty');
            }
        }

        return $this;
    }

    /**
     * Check entry follows length requirement.
     *
     * @param string   $key
     * @param null|int $min
     * @param null|int $max
     *
     * @return Validator
     */
    public function length(string $key, ?int $min, ?int $max = null): self
    {
        $value = $this->getValue($key);
        if (\is_null($value)) {
            return $this;
        }

        $length = \mb_strlen($value);
        if (\is_int($min) &&
            \is_int($max) &&
            ($length < $min || $length > $max)
        ) {
            $this->addError($key, 'length-between', [$min, $max]);

            return $this;
        }

        if (\is_int($min) &&
            ($length < $min)
        ) {
            $this->addError($key, 'length-min', [$min]);
        }

        if (\is_int($max) &&
            $length > $max
        ) {
            $this->addError($key, 'length-max', [$max]);
        }

        return $this;
    }

    /**
     * @param string $key
     * @param string $format
     *
     * @return Validator
     */
    public function datetime(string $key, string $format = 'Y-m-d H:i:s'): self
    {
        $value = $this->getValue($key);
        if (\is_null($value)) {
            return $this;
        }

        $dateTime = DateTime::createFromFormat($format, $value);
        $errors = DateTime::getLastErrors();
        if ($errors['error_count'] > 0 || $errors['warning_count'] > 0 || $dateTime === false) {
            $this->addError($key, 'datetime', [$format]);
        }

        return $this;
    }

    /**
     * Checks entry follows a slug pattern.
     *
     * @param string $key
     *
     * @return Validator
     */
    public function slug(string $key): self
    {
        $value = $this->getValue($key);

        $pattern = '/^[a-z0-9]+(-[a-z0-9]+)*$/';
        if (!\is_null($value) && !\preg_match($pattern, $value)) {
            $this->addError($key, 'slug');
        }

        return $this;
    }

    /**
     * Checks entry exists in the database.
     *
     * @param string $key
     * @param string $table
     * @param PDO    $pdo
     *
     * @return Validator
     */
    public function exists(string $key, string $table, PDO $pdo): self
    {
        $id = $this->getValue($key);
        if (\is_null($id)) {
            return $this;
        }

        $statement = $pdo->prepare("SELECT id FROM {$table} WHERE id = :id");
        $statement->bindParam('id', $id, PDO::PARAM_INT);
        $statement->execute();

        if ($statement->fetchColumn() === false) {
            $this->addError($key, 'exist', [$table]);
        }

        return $this;
    }

    /**
     * Checks key is unique.
     *
     * @param string   $key
     * @param string   $table
     * @param PDO      $pdo
     * @param null|int $exclude
     *
     * @return Validator
     */
    public function unique(string $key, string $table, PDO $pdo, ?int $exclude = null): self
    {
        $value = $this->getValue($key);
        if (\is_null($value)) {
            return $this;
        }

        $query = "SELECT id FROM {$table} WHERE ${key} = :value";
        if (!\is_null($exclude)) {
            $query .= ' AND id <> :id';
        }

        $statement = $pdo->prepare($query);
        $statement->bindParam('value', $value);
        if (!\is_null($exclude)) {
            $statement->bindParam('id', $exclude, PDO::PARAM_INT);
        }
        $statement->execute();

        if ($statement->fetchColumn() !== false) {
            $this->addError($key, 'unique', [$value]);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return empty($this->getErrors());
    }

    /**
     * Retrieve validation errors.
     *
     * @return ValidationError[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Add validation error.
     *
     * @param string $key
     * @param string $rule
     * @param array  $attributes
     */
    private function addError(string $key, string $rule, array $attributes = []): void
    {
        $this->errors[$key] = new ValidationError($key, $rule, $attributes);
    }

    /**
     * @param string $key
     *
     * @return null|mixed
     */
    private function getValue(string $key)
    {
        if (\array_key_exists($key, $this->params)) {
            return $this->params[$key];
        }
    }
}
