<?php

declare(strict_types=1);

namespace Framework\Sessions;

class ArraySessions implements SessionInterface
{
    /**
     * @var array
     */
    private $session = [];

    /**
     * Retrieve information in session storage.
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        if (\array_key_exists($key, $this->session)) {
            return $this->session[$key];
        }

        return $default;
    }

    /**
     * Insert information into session storage.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function set(string $key, $value): void
    {
        $this->session[$key] = $value;
    }

    /**
     * Delete information stored in session.
     *
     * @param string $key
     */
    public function delete(string $key): void
    {
        unset($this->session[$key]);
    }
}
