<?php

declare(strict_types=1);

namespace Framework\Sessions;

interface SessionInterface
{
    /**
     * Retrieve information in session storage.
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null);

    /**
     * Insert information into session storage.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function set(string $key, $value): void;

    /**
     * Delete information stored in session.
     *
     * @param string $key
     */
    public function delete(string $key): void;
}
