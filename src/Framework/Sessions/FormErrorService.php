<?php

declare(strict_types=1);

namespace App\Framework\Sessions;

use Framework\Sessions\SessionInterface;

class FormErrorService
{
    public const SESSION_FORM_INFO_VALUES = 'values';
    public const SESSION_FORM_INFO_ERRORS = 'errors';
    private const SESSION_KEY = 'form-info';

    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function addErrors(iterable $errors): void
    {
        $formInfo = $this->session->get(self::SESSION_KEY, []);
        $formInfo[self::SESSION_FORM_INFO_ERRORS] = $errors;
        $this->session->set(self::SESSION_KEY, $formInfo);
    }

    public function addValues(iterable $values): void
    {
        $formInfo = $this->session->get(self::SESSION_KEY, []);
        $formInfo[self::SESSION_FORM_INFO_VALUES] = $values;
        $this->session->set(self::SESSION_KEY, $formInfo);
    }

    public function get(): iterable
    {
        $formInfo = $this->session->get(
            self::SESSION_KEY,
            [
                self::SESSION_FORM_INFO_VALUES => null,
                self::SESSION_FORM_INFO_ERRORS => null,
            ]
        );
        $this->session->delete(self::SESSION_KEY);

        return $formInfo;
    }
}
