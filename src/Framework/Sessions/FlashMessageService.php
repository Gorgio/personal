<?php

declare(strict_types=1);

namespace Framework\Sessions;

class FlashMessageService
{
    public const SESSION_KEY = 'flash';

    public const ERROR_KEY = 'error';

    public const SUCCESS_KEY = 'success';

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var null|array
     */
    private $messages = null;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function success(string $message): void
    {
        $flash = $this->session->get(self::SESSION_KEY, []);
        $flash[self::SUCCESS_KEY] = $message;
        $this->session->set(self::SESSION_KEY, $flash);
    }

    public function error(string $message): void
    {
        $flash = $this->session->get(self::SESSION_KEY, []);
        $flash[self::ERROR_KEY] = $message;
        $this->session->set(self::SESSION_KEY, $flash);
    }

    /**
     * @param string $type
     *
     * @return null|string
     */
    public function get(string $type): ?string
    {
        if (\is_null($this->messages)) {
            $this->messages = $this->session->get(self::SESSION_KEY, []);
            $this->session->delete(self::SESSION_KEY);
        }

        if (\array_key_exists($type, $this->messages)) {
            return $this->messages[$type];
        }

        return null;
    }
}
