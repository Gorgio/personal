<?php

declare(strict_types=1);

namespace Framework\Twig;

use DateTime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FormExtension extends AbstractExtension
{
    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'field',
                [$this, 'field'],
                [
                    'is_safe' => ['html'],
                    'needs_context' => true,
                ]
            ),
        ];
    }

    /**
     * @param array  $context [] variables passed to the view
     * @param string $name
     * @param mixed  $value
     * @param string $label
     * @param array  $options
     *
     * @return string
     */
    public function field(array $context, string $name, $value, string $label, array $options = [])
    {
        $type = ($options['type'] ?? 'text');
        $value = $this->convertValue($value);
        $error = $this->getErrorHTML($context, $name);

        $attributes = [
            'class' => \trim('form-control ' . ($options['class'] ?? '')),
            'name' => $name,
            'id' => $name,
        ];

        $class = 'form-group';
        if ($error) {
            $class .= ' has-danger';
            $attributes['class'] .= ' form-control-danger';
        }
        if ($type === 'textarea') {
            $field = $this->textarea($value, $attributes);
        } elseif (\array_key_exists('options', $options)) {
            $field = $this->select($value, $options['options'], $attributes);
        } else {
            $field = $this->input($value, $type, $attributes);
        }

        return "<div class=\"{$class}\">
                <label for=\"{$name}\">{$label}</label>
                {$field}
                {$error}
            </div>";
    }

    /**
     * Generate <input type=$type>.
     *
     * @param null|string $value
     * @param string      $type
     * @param array       $attributes
     *
     * @return string
     */
    private function input(?string $value, string $type, array $attributes): string
    {
        return "<input type=\"{$type}\" " . $this->getHTMLFromArray($attributes) . " value=\"{$value}\">";
    }

    /**
     * Generate <textarea>.
     *
     * @param null|string $value
     * @param array       $attributes
     *
     * @return string
     */
    private function textarea(?string $value, array $attributes): string
    {
        return '<textarea ' . $this->getHTMLFromArray($attributes) . ">{$value}</textarea>";
    }

    /**
     * Generate <select>.
     *
     * @param null|string $value
     * @param array       $options
     * @param array       $attributes
     *
     * @return string
     */
    private function select(?string $value, array $options, array $attributes): string
    {
        $htmlOptions = \array_reduce(
            \array_keys($options),
            function (string $html, string $key) use ($value, $options): string {
                $params = ['value' => $key, 'selected' => $key === $value];

                return $html . '<option ' . $this->getHTMLFromArray($params) . '>' . $options[$key] . '</option>';
            },
            ''
        );

        return '<select ' . $this->getHTMLFromArray($attributes) . ">{$htmlOptions}</select>";
    }

    /**
     * @param array  $context
     * @param string $key
     *
     * @return string
     */
    private function getErrorHTML(array $context, string $key): string
    {
        $error = ($context['errors'][$key] ?? false);
        if ($error) {
            return "<small class=\"form-text text-muted\">{$error}</small>";
        }

        return '';
    }

    /**
     * @param array $attributes
     *
     * @return string
     */
    private function getHTMLFromArray(array $attributes): string
    {
        $htmlParts = [];
        foreach ($attributes as $key => $value) {
            if ($value === true) {
                $htmlParts[] = (string) $key;
            }
            if (!\is_bool($value)) {
                $htmlParts[] = "{$key}=\"{$value}\"";
            }
        }

        return \implode(' ', $htmlParts);
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    private function convertValue($value): string
    {
        if ($value instanceof DateTime) {
            return $value->format('Y-m-d H:i:s');
        }

        return (string) $value;
    }
}
