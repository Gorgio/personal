<?php

declare(strict_types=1);

namespace Framework\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Extensions for texts.
 */
class TextExtension extends AbstractExtension
{
    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('excerpt', [$this, 'excerpt']),
        ];
    }

    /**
     * Returns an excerpt from the content.
     *
     * @param string $content
     * @param int    $maxLength
     *
     * @return string
     */
    public function excerpt(string $content, int $maxLength = 100): string
    {
        if (\mb_strlen($content) > $maxLength) {
            $excerpt = \mb_substr($content, 0, $maxLength);
            $lastSpace = \mb_strrpos($excerpt, ' ');
            if ($lastSpace === false) {
                return \mb_substr($content, 0, $maxLength) . '...';
            }

            return \mb_substr($content, 0, $lastSpace) . '...';
        }

        return $content;
    }
}
