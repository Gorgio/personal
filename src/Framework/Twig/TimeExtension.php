<?php

declare(strict_types=1);

namespace Framework\Twig;

use DateTimeInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TimeExtension extends AbstractExtension
{
    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('ago', [$this, 'ago'], ['is_safe' => ['html']]),
        ];
    }

    public function ago(DateTimeInterface $dateTime, string $format = 'd/m/Y H:i'): string
    {
        return '<span class="timeago" datetime="' . $dateTime->format(DateTimeInterface::ISO8601) . '">' .
            $dateTime->format($format) .
            '</span>';
    }
}
