<?php

declare(strict_types=1);

namespace Framework\Twig;

use ParagonIE\AntiCSRF\AntiCSRF;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CSRFFormExtension extends AbstractExtension
{
    /**
     * @var array|\ArrayAccess
     */
    private $session;

    /**
     * CSRFFormExtension constructor.
     *
     * @param array|\ArrayAccess $session
     */
    public function __construct(&$session)
    {
        $this->session = &$session;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'form_token',
                [$this, 'formToken'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    public function formToken(string $lockTo = ''): string
    {
        static $csrf;
        if ($csrf === null) {
            $paramBody = null;
            $serverParam = null;
            $csrf = new AntiCSRF($paramBody, $this->session, $serverParam);
        }

        return $csrf->insertToken($lockTo, false);
    }
}
