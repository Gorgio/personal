<?php

declare(strict_types=1);

namespace Framework\Twig;

use AutoRoute\AutoRoute;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrap4View;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PagerfantaExtension extends AbstractExtension
{
    /**
     * @var AutoRoute
     */
    private $router;

    public function __construct(AutoRoute $router)
    {
        $this->router = $router;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('paginate', [$this, 'paginate'], ['is_safe' => ['html'], 'is_variadic' => true]),
        ];
    }

    /**
     * Generates pagination.
     *
     * @param Pagerfanta $pagerfanta
     * @param string     $className
     * @param array      $params
     *
     * @return string
     */
    public function paginate(
        Pagerfanta $pagerfanta,
        string $className,
        ...$params
    ): string {
        $view = new TwitterBootstrap4View();

        return $view->render(
            $pagerfanta,
            function (int $page) use ($className, $params): ?string {
                if ($page > 1) {
                    $queryArgs['p'] = $page;
                }
                $params[] = $page;

                return $this->router->newGenerator()->generate($className, ...$params);
            }
        );
    }
}
