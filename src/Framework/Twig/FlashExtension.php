<?php

declare(strict_types=1);

namespace App\Framework\Twig;

use Framework\Sessions\FlashMessageService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FlashExtension extends AbstractExtension
{
    /**
     * @var FlashMessageService
     */
    private $flashMessageService;

    public function __construct(FlashMessageService $flashMessageService)
    {
        $this->flashMessageService = $flashMessageService;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('flash', [$this, 'getFlash']),
        ];
    }

    /**
     * @param string $type
     *
     * @return null|string
     */
    public function getFlash(string $type): ?string
    {
        return $this->flashMessageService->get($type);
    }
}
