<?php

declare(strict_types=1);

namespace App\Framework\Twig;

use AutoRoute\AutoRoute;
use AutoRoute\NotFound;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AutoRouteGeneratorExtension extends AbstractExtension
{
    /**
     * @var AutoRoute
     */
    private $autoRoute;

    public function __construct(AutoRoute $autoRoute)
    {
        $this->autoRoute = $autoRoute;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('path', [$this, 'getPathForClassRoute'], ['is_variadic' => true]),
        ];
    }

    /**
     * @param class-string $className
     * @param mixed        ...$params
     *
     * @throws NotFound
     *
     * @return string
     */
    public function getPathForClassRoute(string $className, ...$params): string
    {
        return $this->autoRoute->newGenerator()->generate($className, ...$params);
    }
}
