<?php

declare(strict_types=1);

namespace Framework\Router;

/**
 * Class Route
 * Represents a matched route.
 */
class Route
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var callable
     */
    private $callback;
    /**
     * @var array
     */
    private $parameters;

    /**
     * Route constructor.
     *
     * @param string          $name
     * @param callable|string $callback
     * @param array           $parameters
     */
    public function __construct(string $name, $callback, array $parameters)
    {
        $this->name = $name;
        $this->callback = $callback;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return callable|string
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * Retrieve URI parameters.
     *
     * @return string[]
     */
    public function getParams(): array
    {
        return $this->parameters;
    }
}
