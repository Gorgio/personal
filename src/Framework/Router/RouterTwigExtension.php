<?php

declare(strict_types=1);

namespace Framework\Router;

use Framework\Router;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RouterTwigExtension extends AbstractExtension
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var ServerRequestInterface
     */
    private $request;

    public function __construct(Router $router, ServerRequestInterface $request)
    {
        $this->router = $router;
        $this->request = $request;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('path', [$this, 'pathFor']),
            new TwigFunction('is_sub_path', [$this, 'isSubPath']),
        ];
    }

    /**
     * @param string $path
     * @param array  $params
     *
     * @return null|string
     */
    public function pathFor(string $path, array $params = []): ?string
    {
        return $this->router->generateUri($path, $params);
    }

    /**
     * @param string $route
     *
     * @return bool
     */
    public function isSubPath(string $route): bool
    {
        $uri = (string) ($this->request->getUri() ?? '/');
        $expectedUri = $this->router->generateUri($route);
        if (\is_null($expectedUri)) {
            return false;
        }

        return \strpos($uri, $expectedUri) !== false;
    }
}
