<?php

declare(strict_types=1);

namespace App\Framework;

use AutoRoute\AutoRoute;
use AutoRoute\Generator;
use AutoRoute\NotFound;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class AutoRouteRedirect
{
    private const HTTP_MOVED_PERMANENTLY = 301;
    private const HTTP_FOUND = 302;
    private const HTTP_SEE_OTHER = 303;
    private const HTTP_TEMPORARY_REDIRECT = 307;
    private const HTTP_PERMANENT_REDIRECT = 308;

    /**
     * @var AutoRoute
     */
    private $router;

    public function __construct(AutoRoute $router)
    {
        $this->router = $router;
    }

    /**
     * Permanent Redirection Response HTTP 308 (Permanent redirection no HTTP method change allowed).
     *
     * @param class-string $class
     * @param mixed        $params
     *
     * @throws NotFound
     *
     * @return ResponseInterface
     */
    public function redirectPermanent(string $class, ...$params): ResponseInterface
    {
        return $this->redirect(self::HTTP_PERMANENT_REDIRECT, $class, ...$params);
    }

    /**
     * @param class-string $class
     * @param mixed        ...$params
     *
     * @throws NotFound
     *
     * @return ResponseInterface
     */
    public function redirectTemporary(string $class, ...$params): ResponseInterface
    {
        return $this->redirect(self::HTTP_TEMPORARY_REDIRECT, $class, ...$params);
    }

    /**
     * @param class-string $class
     * @param mixed        ...$params
     *
     * @throws NotFound
     *
     * @return ResponseInterface
     */
    public function redirectSeeOther(string $class, ...$params): ResponseInterface
    {
        return $this->redirect(self::HTTP_SEE_OTHER, $class, ...$params);
    }

    /**
     * @param class-string $class
     * @param mixed        ...$params
     *
     * @throws NotFound
     *
     * @return ResponseInterface
     */
    public function redirectPermanentAllowHTTPMethodChange(string $class, ...$params): ResponseInterface
    {
        return $this->redirect(self::HTTP_MOVED_PERMANENTLY, $class, ...$params);
    }

    /**
     * @param class-string $class
     * @param mixed        ...$params
     *
     * @throws NotFound
     *
     * @return ResponseInterface
     */
    public function redirectTemporaryAllowHTTPMethodChange(string $class, ...$params): ResponseInterface
    {
        return $this->redirect(self::HTTP_FOUND, $class, ...$params);
    }

    /**
     * @param int          $status
     * @param class-string $class
     * @param mixed        ...$params
     *
     * @throws NotFound
     *
     * @return ResponseInterface
     */
    private function redirect(int $status, string $class, ...$params): ResponseInterface
    {
        $redirectedURI = $this->getRouteGenerator()->generate($class, ...$params);

        return (new Response())
            ->withStatus($status)
            ->withHeader('Location', $redirectedURI)
        ;
    }

    private function getRouteGenerator(): Generator
    {
        return $this->router->newGenerator();
    }
}
