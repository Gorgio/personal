<?php

declare(strict_types=1);

namespace App\Framework\Middleware;

use AutoRoute\AutoRoute;
use GuzzleHttp\Psr7\Response;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AutoRouteMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var AutoRoute
     */
    private $autoRoute;

    /**
     * @param AutoRoute          $autoRoute
     * @param ContainerInterface $container
     */
    public function __construct(AutoRoute $autoRoute, ContainerInterface $container)
    {
        $this->container = $container;
        $this->autoRoute = $autoRoute;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     *
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $router = $this->autoRoute->newRouter();

        try {
            $route = $router->route($request->getMethod(), $request->getUri()->getPath());

            $action = $this->container->get($route->class);
            $method = $route->method;

            return $action->{$method}(...$route->params);
        } catch (\AutoRoute\InvalidNamespace $e) {
            // 400 Bad Request
            return new Response(400, [], '<h1>HTTP 400: Bad Request</h1>');
        } catch (\AutoRoute\InvalidArgument $e) {
            // 400 Bad Request
            return new Response(400, [], '<h1>HTTP 400: Bad Request</h1>');
        } catch (\AutoRoute\NotFound $e) {
            // 404 Not Found
            return new Response(404, [], '<h1>HTTP 404: Not Found</h1>');
        } catch (\AutoRoute\MethodNotAllowed $e) {
            // 405 Method Not Allowed
            return new Response(405, [], '<h1>HTTP 405: Method Not Allowed</h1>');
        }
    }
}
