<?php

declare(strict_types=1);

namespace App\Framework\Middleware;

use ArrayAccess;
use GuzzleHttp\Psr7\Response;
use ParagonIE\AntiCSRF\AntiCSRF;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TypeError;

class CSRFMiddleware implements MiddlewareInterface
{
    /**
     * @var array|\ArrayAccess
     */
    private $session;

    /**
     * CSRFMiddleware constructor.
     *
     * @param array|ArrayAccess $session
     */
    public function __construct(&$session)
    {
        if (!$this->isValidSession($session)) {
            throw new TypeError('Session passed to middleware is unusable');
        }
        $this->session = &$session;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * Return an HTTP 400 error when the CSRF check fails.
     *
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (\in_array($request->getMethod(), ['DELETE', 'PATCH', 'POST', 'PUT'], true)) {
            $requestBody = $request->getParsedBody();
            if (\is_object($requestBody)) {
                $requestBody = [];
            }
            $session = &$this->session;
            $serverParams = $request->getServerParams();

            $csrft = new AntiCSRF($requestBody, $session, $serverParams);
            if ($csrft->validateRequest() === false) {
                return new Response(400);
            }
        }

        return $handler->handle($request);
    }

    /**
     * @param mixed $session
     *
     * @return bool
     */
    private function isValidSession($session): bool
    {
        if (!\is_array($session) && !$session instanceof ArrayAccess) {
            return false;
        }

        return true;
    }
}
