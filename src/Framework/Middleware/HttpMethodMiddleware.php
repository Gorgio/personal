<?php

declare(strict_types=1);

namespace App\Framework\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class HttpMethodMiddleware implements MiddlewareInterface
{
    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $parsedBody = $request->getParsedBody();

        if (\is_array($parsedBody) &&
            \array_key_exists('_METHOD', $parsedBody) &&
            \in_array($parsedBody['_METHOD'], ['DELETE', 'delete', 'PATCH', 'patch', 'PUT', 'put'], true)
        ) {
            $request = $request->withMethod(\strtoupper($parsedBody['_METHOD']));
        }

        return $handler->handle($request);
    }
}
