<?php

declare(strict_types=1);

namespace App\Domain\Blog;

use App\Blog\Entities\Post;
use App\Blog\Tables\CategoryTable;
use DateTimeImmutable;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class CategoriesModule
{
    public const VIEW_PATH = '@blog/';
    public const ADMIN_VIEW_PATH = '@blog/admin/categories/';

    public const FLASH_MESSAGES = [
        'create' => 'Item has been created.',
        'edit' => 'Item has been edited.',
        'delete' => 'Item has been deleted',
    ];

    /**
     * Params to fetch from request body.
     */
    protected const PARAMS = [
        'name',
        'slug',
    ];

    /**
     * @var CategoryTable
     */
    private $table;

    public function __construct(CategoryTable $categoryTable)
    {
        $this->table = $categoryTable;
    }

    public function getPostEntity()
    {
        $post = new Post();
        $post->setCreatedAt(new DateTimeImmutable());

        return $post;
    }

    public function getParams(ServerRequestInterface $request)
    {
        return \array_filter(
            $this->getRequestBody($request),
            static function (string $key): bool {
                return \in_array($key, static::PARAMS, true);
            },
            \ARRAY_FILTER_USE_KEY
        );
    }

    /**
     * Method to inject parameters to the view.
     *
     * @param array $params
     *
     * @return array
     */
    public function getParamsForForm(array $params): array
    {
        $params['categories'] = $this->table->findList();

        return $params;
    }

    /**
     * Generates validator for input validation.
     *
     * @param ServerRequestInterface $request
     * @param null|int               $id
     *
     * @return Validator
     */
    public function getValidator(ServerRequestInterface $request, ?int $id = null): Validator
    {
        $validator = new Validator($this->getRequestBody($request));
        $validator
            ->required('name', 'slug')
            ->length('name', 5, 250)
            ->unique('name', $this->table->getTable(), $this->table->getPdo(), $id)
            ->length('slug', 2, 50)
            ->unique('slug', $this->table->getTable(), $this->table->getPdo(), $id)
            ->slug('slug')
        ;

        return $validator;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return array
     */
    private function getRequestBody(ServerRequestInterface $request): array
    {
        $requestBody = $request->getParsedBody();
        if (!\is_array($requestBody)) {
            $requestBody = [];
        }

        return $requestBody;
    }
}
