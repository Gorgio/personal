<?php

declare(strict_types=1);

namespace App\Blog\Tables;

use App\Blog\Entities\Post;
use Framework\Database\PaginatedQuery;
use Framework\Database\Table;
use Pagerfanta\Pagerfanta;

class PostTable extends Table
{
    /**
     * @var string
     */
    protected const TABLE_NAME = 'posts';
    /**
     * @var class-string
     */
    protected $entity = Post::class;

    /**
     * @param int $id
     *
     * @throws \Framework\Database\NoRecordException
     *
     * @return Post
     */
    public function findWithCategory(int $id): Post
    {
        return $this->fetchOrFail(
            'SELECT p.*, c.name AS category_name, c.slug AS category_slug
            FROM ' . static::TABLE_NAME . ' AS p
            LEFT JOIN categories AS c
            ON c.id = category_id
            WHERE p.id = :id',
            ['id' => $id]
        );
    }

    /**
     * @param int $perPage
     * @param int $currentPage
     *
     * @return Pagerfanta
     */
    public function findPaginated(int $perPage, int $currentPage): Pagerfanta
    {
        $query = new PaginatedQuery(
            $this->getPdo(),
            'SELECT p.*, c.name AS category_name, c.slug AS category_slug
            FROM posts AS p
            LEFT JOIN categories AS c
            ON c.id = p.category_id
            ORDER BY p.created_at DESC',
            'SELECT COUNT(id) FROM ' . static::TABLE_NAME,
            $this->entity
        );

        return (new Pagerfanta($query))
            ->setMaxPerPage($perPage)
            ->setCurrentPage($currentPage)
        ;
    }

    public function findPaginatedPublicForCategory(int $perPage, int $currentPage, int $categoryId): Pagerfanta
    {
        $query = new PaginatedQuery(
            $this->getPdo(),
            'SELECT p.*, c.name AS category_name, c.slug AS category_slug
            FROM posts AS p
            LEFT JOIN categories AS c
            ON c.id = p.category_id
            WHERE p.category_id = :category_id
            ORDER BY p.created_at DESC',
            'SELECT COUNT(id) FROM ' . static::TABLE_NAME . ' WHERE category_id = :category_id',
            $this->entity,
            ['category_id' => $categoryId]
        );

        return (new Pagerfanta($query))
            ->setMaxPerPage($perPage)
            ->setCurrentPage($currentPage)
        ;
    }
}
