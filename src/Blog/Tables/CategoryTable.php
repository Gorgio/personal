<?php

declare(strict_types=1);

namespace App\Blog\Tables;

use App\Blog\Entities\Category;
use Framework\Database\Table;

class CategoryTable extends Table
{
    /**
     * @var string
     */
    protected const TABLE_NAME = 'categories';

    /**
     * @var class-string
     */
    protected $entity = Category::class;
}
