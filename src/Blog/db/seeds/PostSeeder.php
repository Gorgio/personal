<?php

declare(strict_types=1);

use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class PostSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run(): void
    {
        // Category seeding
        $data = [];
        $faker = Factory::create('en_GB');
        for ($i = 0; $i < 5; ++$i) {
            $data[] = [
                'name' => $faker->catchPhrase,
                'slug' => $faker->slug,
            ];
        }

        $this->table('categories')
            ->insert($data)
            ->save()
        ;

        // Article seeding
        $data = [];
        for ($i = 0; $i < 100; ++$i) {
            $date = \date('Y-m-d H:i:s', $faker->unixTime('now'));
            $data[] = [
                'name' => $faker->catchPhrase,
                'slug' => $faker->slug,
                'category_id' => \mt_rand(1, 5),
                'content' => $faker->text(3000),
                'created_at' => $date,
                'updated_at' => $date,
            ];
        }

        $this->table('posts')
            ->insert($data)
            ->save()
        ;
    }
}
