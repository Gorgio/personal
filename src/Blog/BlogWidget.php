<?php

declare(strict_types=1);

namespace App\Blog;

use App\Admin\AdminWidgetInterface;
use App\Blog\Tables\PostTable;
use Framework\Renderer\RendererInterface;

class BlogWidget implements AdminWidgetInterface
{
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var PostTable
     */
    private $postTable;

    public function __construct(RendererInterface $renderer, PostTable $postTable)
    {
        $this->renderer = $renderer;
        $this->postTable = $postTable;
    }

    public function render(): string
    {
        $postCount = $this->postTable->count();

        return $this->renderer->render('@blog/admin/widget', ['count' => $postCount]);
    }

    public function renderMenu(): string
    {
        return $this->renderer->render('@blog/admin/menu');
    }
}
