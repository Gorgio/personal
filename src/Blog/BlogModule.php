<?php

declare(strict_types=1);

namespace App\Blog;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Psr\Container\ContainerInterface;

class BlogModule extends Module
{
    public const DEFINITIONS = __DIR__ . \DIRECTORY_SEPARATOR . 'config.php';

    public const MIGRATIONS = __DIR__ . \DIRECTORY_SEPARATOR . 'db/migrations';

    public const SEEDS = __DIR__ . \DIRECTORY_SEPARATOR . 'db/seeds';

    public function __construct(ContainerInterface $container)
    {
        $container->get(RendererInterface::class)->addPath(
            'blog',
            __DIR__ . \DIRECTORY_SEPARATOR . 'views'
        );
    }
}
