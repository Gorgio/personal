<?php

declare(strict_types=1);

namespace App\Blog\Entities;

use DateTimeImmutable;
use DateTimeInterface;
use Framework\Entities\Entity;
use InvalidArgumentException;

class Post extends Entity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $content;

    /**
     * @var DateTimeInterface
     */
    private $createdAt;

    /**
     * @var DateTimeInterface
     */
    private $updatedAt;

    /**
     * @var ?Category
     */
    private $category;

    public function __construct()
    {
        $this->setId((int) $this->id);
    }

    public function __set(string $property, $value): void
    {
        if (\strncmp($property, 'category_', 9) === 0) {
            if (\is_null($this->category)) {
                $this->category = new Category();
            }
            $property = \substr($property, 9);
            if ($property === 'id') {
                $this->category->setId((int) $value);

                return;
            }
            $method = 'set' . \ucfirst($property);
            $this->category->{$method}($value);

            return;
        }
        parent::__set($property, $value);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getCreatedAtString(): ?string
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }

    /**
     * @param DateTimeInterface|string $datetime
     *
     * @throws \InvalidArgumentException
     */
    public function setCreatedAt($datetime): void
    {
        if (\is_string($datetime)) {
            $datetime = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $datetime);
        }
        if (($datetime instanceof DateTimeInterface) === false) {
            throw new InvalidArgumentException('CreatedAt property needs to be passed a DateTimeInterface
            or a string which can be turned into a DateTimeInterface');
        }
        $this->createdAt = $datetime;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAtString(): ?string
    {
        return $this->updatedAt->format('Y-m-d H:i:s');
    }

    /**
     * /**
     * @param DateTimeInterface|string $datetime
     *
     * @throws \InvalidArgumentException
     */
    public function setUpdatedAt($datetime): void
    {
        if (\is_string($datetime)) {
            $datetime = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $datetime);
        }
        if (($datetime instanceof DateTimeInterface) === false) {
            throw new InvalidArgumentException('UpdatedAt property needs to be passed a DateTimeInterface
            or a string which can be turned into a DateTimeInterface');
        }
        $this->updatedAt = $datetime;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }
}
