<?php

declare(strict_types=1);

namespace App\Admin;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AdminTwigExtension extends AbstractExtension
{
    /**
     * @var array
     */
    private $widgets;

    public function __construct(array $widgets)
    {
        $this->widgets = $widgets;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('admin_menu', [$this, 'renderMenu'], ['is_safe' => ['html']]),
        ];
    }

    public function renderMenu(): string
    {
        return \array_reduce(
            $this->widgets,
            static function (string $html, AdminWidgetInterface $widget): string {
                return $html . $widget->renderMenu();
            },
            ''
        );
    }
}
