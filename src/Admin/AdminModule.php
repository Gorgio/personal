<?php

declare(strict_types=1);

namespace App\Admin;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Renderer\TwigRenderer;

class AdminModule extends Module
{
    public const DEFINITIONS = __DIR__ . \DIRECTORY_SEPARATOR . 'config.php';

    public function __construct(
        RendererInterface $renderer,
        AdminTwigExtension $adminTwigExtension,
        string $prefix
    ) {
        $renderer->addPath('admin', __DIR__ . DS . 'views');
        if ($renderer instanceof TwigRenderer) {
            $renderer->getTwig()->addExtension($adminTwigExtension);
        }
    }
}
