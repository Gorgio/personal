<?php

declare(strict_types=1);

use App\Admin\AdminModule;
use App\Admin\AdminTwigExtension;

return [
    'admin.prefix' => '/admin',
    AdminTwigExtension::class => \DI\autowire()->constructorParameter('widgets', \DI\get('admin.widgets')),
    'admin.widgets' => [],
    AdminModule::class => \DI\autowire()->constructorParameter('prefix', \DI\get('admin.prefix')),
    \App\HTTP\Admin\GetAdmin::class => \DI\autowire()->constructorParameter('widgets', \DI\get('admin.widgets')),
];
