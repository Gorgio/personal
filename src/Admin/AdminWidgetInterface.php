<?php

declare(strict_types=1);

namespace App\Admin;

interface AdminWidgetInterface
{
    public function render(): string;

    public function renderMenu(): string;
}
