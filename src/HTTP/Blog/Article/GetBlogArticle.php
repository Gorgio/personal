<?php

declare(strict_types=1);

namespace App\HTTP\Blog\Article;

use App\Blog\Tables\PostTable;
use App\Domain\Blog\ArticlesModule;
use App\Framework\AutoRouteRedirect;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class GetBlogArticle
{
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var PostTable
     */
    private $postTable;
    /**
     * @var AutoRouteRedirect
     */
    private $routeRedirect;

    public function __construct(
        RendererInterface $renderer,
        PostTable $postTable,
        AutoRouteRedirect $routeRedirect
    ) {
        $this->renderer = $renderer;
        $this->postTable = $postTable;
        $this->routeRedirect = $routeRedirect;
    }

    public function __invoke(int $id, string $slug = ''): ResponseInterface
    {
        try {
            $post = $this->postTable->findWithCategory($id);
        } catch (NoRecordException $exception) {
            return new Response(404);
        }

        if ($post->getSlug() !== $slug) {
            return $this->routeRedirect->redirectPermanent(self::class, $post->getId(), $post->getSlug());
        }

        return new Response(
            200,
            [],
            $this->renderer->render(ArticlesModule::VIEW_PATH . 'show', ['post' => $post])
        );
    }
}
