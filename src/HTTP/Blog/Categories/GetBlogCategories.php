<?php

declare(strict_types=1);

namespace App\HTTP\Blog\Categories;

use App\Blog\Tables\CategoryTable;
use App\Blog\Tables\PostTable;
use App\Domain\Blog\CategoriesModule;
use Framework\Renderer\RendererInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class GetBlogCategories
{
    private const CATEGORIES_PER_PAGE = 10;

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var CategoryTable
     */
    private $categoryTable;

    /**
     * @var PostTable
     */
    private $postTable;

    public function __construct(
        RendererInterface $renderer,
        CategoryTable $categoryTable,
        PostTable $postTable
    ) {
        $this->renderer = $renderer;
        $this->categoryTable = $categoryTable;
        $this->postTable = $postTable;
    }

    public function __invoke(string $slug, int $page = 1): ResponseInterface
    {
        $category = $this->categoryTable->findBy('slug', $slug);
        $posts = $this->postTable->findPaginatedPublicForCategory(
            self::CATEGORIES_PER_PAGE,
            $page,
            (int) $category->getId()
        );
        $categories = $this->categoryTable->findAll();

        return new Response(
            200,
            [],
            $this->renderer->render(
                CategoriesModule::VIEW_PATH . 'index',
                [
                    'posts' => $posts,
                    'category' => $category,
                    'categories' => $categories,
                    'page' => $page,
                ]
            )
        );
    }
}
