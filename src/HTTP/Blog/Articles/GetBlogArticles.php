<?php

declare(strict_types=1);

namespace App\HTTP\Blog\Articles;

use App\Blog\Tables\CategoryTable;
use App\Blog\Tables\PostTable;
use App\Domain\Blog\ArticlesModule;
use Framework\Renderer\RendererInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class GetBlogArticles
{
    private const ARTICLE_PER_PAGE = 10;

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var PostTable
     */
    private $postTable;

    /**
     * @var CategoryTable
     */
    private $categoryTable;

    public function __construct(
        RendererInterface $renderer,
        PostTable $postTable,
        CategoryTable $categoryTable
    ) {
        $this->renderer = $renderer;
        $this->postTable = $postTable;
        $this->categoryTable = $categoryTable;
    }

    public function __invoke(int $page = 1): ResponseInterface
    {
        $posts = $this->postTable->findPaginated(static::ARTICLE_PER_PAGE, $page);
        $categories = $this->categoryTable->findAll();

        return new Response(
            200,
            [],
            $this->renderer->render(
                ArticlesModule::VIEW_PATH . 'index',
                [
                    'posts' => $posts,
                    'categories' => $categories,
                    'page' => $page,
                ]
            )
        );
    }
}
