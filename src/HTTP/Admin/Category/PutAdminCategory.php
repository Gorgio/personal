<?php

declare(strict_types=1);

namespace App\HTTP\Admin\Category;

use App\Blog\Tables\CategoryTable;
use App\Domain\Blog\CategoriesModule;
use App\Framework\AutoRouteRedirect;
use App\Framework\Sessions\FormErrorService;
use App\HTTP\Admin\Categories\GetAdminCategories;
use App\HTTP\Admin\Category\Edit\GetAdminCategoryEdit;
use AutoRoute\NotFound;
use Framework\Database\NoRecordException;
use Framework\Sessions\FlashMessageService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class PutAdminCategory
{
    /**
     * @var ServerRequestInterface
     */
    private $request;
    /**
     * @var CategoriesModule
     */
    private $module;
    /**
     * @var CategoryTable
     */
    private $table;
    /**
     * @var FlashMessageService
     */
    private $flashMessageService;
    /**
     * @var AutoRouteRedirect
     */
    private $autoRouteRedirect;
    /**
     * @var FormErrorService
     */
    private $formErrorService;

    public function __construct(
        ServerRequestInterface $request,
        CategoriesModule $module,
        CategoryTable $table,
        AutoRouteRedirect $autoRouteRedirect,
        FlashMessageService $flashMessageService,
        FormErrorService $formErrorService
    ) {
        $this->request = $request;
        $this->module = $module;
        $this->table = $table;
        $this->flashMessageService = $flashMessageService;
        $this->autoRouteRedirect = $autoRouteRedirect;
        $this->formErrorService = $formErrorService;
    }

    /**
     * @param int $id
     *
     * @throws NotFound
     * @throws NoRecordException
     *
     * @return ResponseInterface
     */
    public function __invoke(int $id): ResponseInterface
    {
        $item = $this->table->find($id);
        $params = $this->module->getParams($this->request);
        $validator = $this->module->getValidator($this->request, $id);

        if (!$validator->isValid()) {
            $params['id'] = $id;
            $this->formErrorService->addValues($params);
            $this->formErrorService->addErrors($validator->getErrors());

            return $this->autoRouteRedirect->redirectSeeOther(GetAdminCategoryEdit::class, $id);
        }

        $this->table->update($item->getId(), $params);
        $this->flashMessageService->success(CategoriesModule::FLASH_MESSAGES['edit']);

        return $this->autoRouteRedirect->redirectSeeOther(GetAdminCategories::class);
    }
}
