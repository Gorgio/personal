<?php

declare(strict_types=1);

namespace App\HTTP\Admin\Category;

use App\Blog\Tables\CategoryTable;
use App\Domain\Blog\CategoriesModule;
use App\Framework\AutoRouteRedirect;
use App\HTTP\Admin\Categories\GetAdminCategories;
use Framework\Sessions\FlashMessageService;
use Psr\Http\Message\ResponseInterface;

class DeleteAdminCategory
{
    /**
     * @var CategoriesModule
     */
    private $module;
    /**
     * @var CategoryTable
     */
    private $table;
    /**
     * @var FlashMessageService
     */
    private $flashMessageService;
    /**
     * @var AutoRouteRedirect
     */
    private $autoRouteRedirect;

    public function __construct(
        CategoriesModule $module,
        CategoryTable $table,
        AutoRouteRedirect $autoRouteRedirect,
        FlashMessageService $flashMessageService
    ) {
        $this->module = $module;
        $this->flashMessageService = $flashMessageService;
        $this->autoRouteRedirect = $autoRouteRedirect;
        $this->table = $table;
    }

    public function __invoke(int $id): ResponseInterface
    {
        $this->table->delete($id);
        $this->flashMessageService->success(CategoriesModule::FLASH_MESSAGES['delete']);

        return $this->autoRouteRedirect->redirectSeeOther(GetAdminCategories::class);
    }
}
