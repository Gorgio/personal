<?php

declare(strict_types=1);

namespace App\HTTP\Admin\Category\Add;

use App\Domain\Blog\CategoriesModule;
use App\Framework\Sessions\FormErrorService;
use Framework\Renderer\RendererInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class GetAdminCategoryAdd
{
    /**
     * @var CategoriesModule
     */
    private $module;
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var FormErrorService
     */
    private $formErrorService;

    public function __construct(
        RendererInterface $renderer,
        CategoriesModule $module,
        FormErrorService $formErrorService
    ) {
        $this->module = $module;
        $this->renderer = $renderer;
        $this->formErrorService = $formErrorService;
    }

    /**
     * @return ResponseInterface
     */
    public function __invoke(): ResponseInterface
    {
        $httpStatusCode = 200;
        [
            FormErrorService::SESSION_FORM_INFO_VALUES => $item,
            FormErrorService::SESSION_FORM_INFO_ERRORS => $errors
        ] = $this->formErrorService->get();
        if (\is_null($item)) {
            $item = $this->module->getPostEntity();
        }

        if (!\is_null($errors)) {
            $httpStatusCode = 422;
        }

        $formParams = $this->module->getParamsForForm(['item' => $item, 'errors' => $errors]);

        return new Response(
            $httpStatusCode,
            [],
            $this->renderer->render(CategoriesModule::ADMIN_VIEW_PATH . 'create', $formParams)
        );
    }
}
