<?php

declare(strict_types=1);

namespace App\HTTP\Admin\Categories;

use App\Blog\Tables\CategoryTable;
use App\Domain\Blog\CategoriesModule;
use Framework\Renderer\RendererInterface;
use Framework\Sessions\FlashMessageService;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class GetAdminCategories
{
    private const CATEGORIES_PER_PAGE = 10;

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var CategoryTable
     */
    private $table;
    /**
     * @var FlashMessageService
     */
    private $flashMessageService;

    public function __construct(
        RendererInterface $renderer,
        CategoryTable $categoryTable,
        FlashMessageService $flashMessageService
    ) {
        $this->renderer = $renderer;
        $this->table = $categoryTable;
        $this->flashMessageService = $flashMessageService;
    }

    public function __invoke(int $page = 1): ResponseInterface
    {
        $categories = $this->table->findPaginated(static::CATEGORIES_PER_PAGE, $page);

        return new Response(
            200,
            [],
            $this->renderer->render(
                CategoriesModule::ADMIN_VIEW_PATH . 'index',
                [
                    'items' => $categories,
                    'session' => $this->flashMessageService,
                    'page' => $page,
                ]
            )
        );
    }
}
