<?php

declare(strict_types=1);

namespace App\HTTP\Admin\Article\Edit;

use App\Blog\Tables\PostTable;
use App\Domain\Blog\ArticlesModule;
use App\Framework\Sessions\FormErrorService;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class GetAdminArticleEdit
{
    /**
     * @var ArticlesModule
     */
    private $module;
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var FormErrorService
     */
    private $formErrorService;
    /**
     * @var PostTable
     */
    private $table;

    public function __construct(
        RendererInterface $renderer,
        ArticlesModule $module,
        PostTable $table,
        FormErrorService $formErrorService
    ) {
        $this->module = $module;
        $this->renderer = $renderer;
        $this->formErrorService = $formErrorService;
        $this->table = $table;
    }

    /**
     * @param int $id
     *
     * @throws NoRecordException
     *
     * @return Response
     */
    public function __invoke(int $id): ResponseInterface
    {
        $httpStatusCode = 200;
        [
            FormErrorService::SESSION_FORM_INFO_VALUES => $item,
            FormErrorService::SESSION_FORM_INFO_ERRORS => $errors
        ] = $this->formErrorService->get();
        if (\is_null($item)) {
            $item = $this->table->find($id);
        }

        if (!\is_null($errors)) {
            $httpStatusCode = 422;
        }

        $formParams = $this->module->getParamsForForm(['item' => $item, 'errors' => $errors]);

        return new Response(
            $httpStatusCode,
            [],
            $this->renderer->render(ArticlesModule::ADMIN_VIEW_PATH . 'edit', $formParams)
        );
    }
}
