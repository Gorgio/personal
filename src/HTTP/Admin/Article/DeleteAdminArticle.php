<?php

declare(strict_types=1);

namespace App\HTTP\Admin\Article;

use App\Blog\Tables\PostTable;
use App\Domain\Blog\ArticlesModule;
use App\Framework\AutoRouteRedirect;
use App\HTTP\Admin\Articles\GetAdminArticles;
use Framework\Sessions\FlashMessageService;
use Psr\Http\Message\ResponseInterface;

class DeleteAdminArticle
{
    /**
     * @var ArticlesModule
     */
    private $module;
    /**
     * @var PostTable
     */
    private $table;
    /**
     * @var FlashMessageService
     */
    private $flashMessageService;
    /**
     * @var AutoRouteRedirect
     */
    private $autoRouteRedirect;

    public function __construct(
        ArticlesModule $module,
        PostTable $table,
        AutoRouteRedirect $autoRouteRedirect,
        FlashMessageService $flashMessageService
    ) {
        $this->module = $module;
        $this->flashMessageService = $flashMessageService;
        $this->autoRouteRedirect = $autoRouteRedirect;
        $this->table = $table;
    }

    public function __invoke(int $id): ResponseInterface
    {
        $this->table->delete($id);
        $this->flashMessageService->success(ArticlesModule::FLASH_MESSAGES['delete']);

        return $this->autoRouteRedirect->redirectSeeOther(GetAdminArticles::class);
    }
}
