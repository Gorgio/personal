<?php

declare(strict_types=1);

namespace App\HTTP\Admin\Article;

use App\Blog\Tables\PostTable;
use App\Domain\Blog\ArticlesModule;
use App\Framework\AutoRouteRedirect;
use App\Framework\Sessions\FormErrorService;
use App\HTTP\Admin\Article\Add\GetAdminArticleAdd;
use App\HTTP\Admin\Articles\GetAdminArticles;
use AutoRoute\NotFound;
use Framework\Sessions\FlashMessageService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class PostAdminArticle
{
    /**
     * @var ServerRequestInterface
     */
    private $request;
    /**
     * @var ArticlesModule
     */
    private $module;
    /**
     * @var PostTable
     */
    private $table;
    /**
     * @var FlashMessageService
     */
    private $flashMessageService;
    /**
     * @var AutoRouteRedirect
     */
    private $autoRouteRedirect;
    /**
     * @var FormErrorService
     */
    private $formErrorService;

    public function __construct(
        ServerRequestInterface $request,
        ArticlesModule $module,
        PostTable $table,
        AutoRouteRedirect $autoRouteRedirect,
        FlashMessageService $flashMessageService,
        FormErrorService $formErrorService
    ) {
        $this->request = $request;
        $this->module = $module;
        $this->table = $table;
        $this->flashMessageService = $flashMessageService;
        $this->autoRouteRedirect = $autoRouteRedirect;
        $this->formErrorService = $formErrorService;
    }

    /**
     * @throws NotFound
     *
     * @return ResponseInterface
     */
    public function __invoke(): ResponseInterface
    {
        $params = $this->module->getParams($this->request);
        $validator = $this->module->getValidator($this->request);

        if (!$validator->isValid()) {
            $this->formErrorService->addValues($params);
            $this->formErrorService->addErrors($validator->getErrors());

            return $this->autoRouteRedirect->redirectSeeOther(GetAdminArticleAdd::class);
        }

        $this->table->insert($params);
        $this->flashMessageService->success(ArticlesModule::FLASH_MESSAGES['create']);

        return $this->autoRouteRedirect->redirectSeeOther(GetAdminArticles::class);
    }
}
