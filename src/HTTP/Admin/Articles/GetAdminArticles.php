<?php

declare(strict_types=1);

namespace App\HTTP\Admin\Articles;

use App\Blog\Tables\CategoryTable;
use App\Blog\Tables\PostTable;
use App\Domain\Blog\ArticlesModule;
use Framework\Renderer\RendererInterface;
use Framework\Sessions\FlashMessageService;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class GetAdminArticles
{
    private const ARTICLES_PER_PAGE = 10;

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var PostTable
     */
    private $postTable;

    /**
     * @var CategoryTable
     */
    private $categoryTable;
    /**
     * @var FlashMessageService
     */
    private $flashMessageService;

    public function __construct(
        RendererInterface $renderer,
        PostTable $postTable,
        CategoryTable $categoryTable,
        FlashMessageService $flashMessageService
    ) {
        $this->renderer = $renderer;
        $this->postTable = $postTable;
        $this->categoryTable = $categoryTable;
        $this->flashMessageService = $flashMessageService;
    }

    public function __invoke(int $page = 1): ResponseInterface
    {
        $posts = $this->postTable->findPaginated(static::ARTICLES_PER_PAGE, $page);
        $categories = $this->categoryTable->findAll();

        return new Response(
            200,
            [],
            $this->renderer->render(
                ArticlesModule::ADMIN_VIEW_PATH . 'index',
                [
                    'items' => $posts,
                    'categories' => $categories,
                    'page' => $page,
                    'session' => $this->flashMessageService,
                ]
            )
        );
    }
}
