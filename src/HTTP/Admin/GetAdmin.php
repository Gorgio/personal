<?php

declare(strict_types=1);

namespace App\HTTP\Admin;

use App\Admin\AdminWidgetInterface;
use Framework\Renderer\RendererInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class GetAdmin
{
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var AdminWidgetInterface[]
     */
    private $widgets;

    public function __construct(RendererInterface $renderer, array $widgets)
    {
        $this->renderer = $renderer;
        $this->widgets = $widgets;
    }

    public function __invoke(): ResponseInterface
    {
        $widgets = \array_reduce(
            $this->widgets,
            static function (string $html, AdminWidgetInterface $widget): string {
                return $html . $widget->render();
            },
            ''
        );

        return new Response(
            200,
            [],
            $this->renderer->render('@admin/dashboard', ['widgets' => $widgets])
        );
    }
}
