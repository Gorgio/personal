<?php

declare(strict_types=1);

use App\Admin\AdminModule;
use App\Blog\BlogModule;
use App\Framework\Middleware\AutoRouteMiddleware;
use App\Framework\Middleware\CSRFMiddleware;
use App\Framework\Middleware\HttpMethodMiddleware;
use App\Framework\Middleware\TrailingSlashMiddleware;
use Framework\App;
use GuzzleHttp\Psr7\ServerRequest;
use Middlewares\Whoops;

\chdir(\dirname(__DIR__));

require 'vendor/autoload.php';

$env = ($_ENV['ENV'] ?? 'production');
$env = 'dev';

$request = ServerRequest::fromGlobals();

$modules = [
    AdminModule::class,
    BlogModule::class,
];

$app = new App('config' . \DIRECTORY_SEPARATOR, $env);
$app->addModule(AdminModule::class)
    ->addModule(BlogModule::class)
    ->pipe(Whoops::class)
    ->pipe(TrailingSlashMiddleware::class)
    ->pipe(HttpMethodMiddleware::class)
    ->pipe(CSRFMiddleware::class)
    ->pipe(AutoRouteMiddleware::class)
;

if (\PHP_SAPI !== 'cli') {
    $response = $app->run($request);

    Http\Response\send($response);
}
