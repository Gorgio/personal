<?php

declare(strict_types=1);

use App\Framework\Middleware\CSRFMiddleware;
use AutoRoute\AutoRoute;
use function DI\autowire;
use function DI\factory;
use Framework\Renderer\RendererInterface;
use Framework\Renderer\TwigRendererFactory;
use Framework\Sessions\PHPSession;
use Framework\Sessions\SessionInterface;
use Framework\Twig\CSRFFormExtension;

return [
    'env' => \DI\env('ENV', 'production'),
    'database.host' => 'localhost',
    'database.username' => 'girgias',
    'database.password' => 'Girgias1&',
    'database.name' => 'personal',
    'views.path' => \dirname(__DIR__) . \DIRECTORY_SEPARATOR . 'views',
    SessionInterface::class => autowire(PHPSession::class),
    CSRFMiddleware::class => autowire()->constructorParameter('session', DI\get(SessionInterface::class)),
    CSRFFormExtension::class => autowire()->constructorParameter('session', DI\get(SessionInterface::class)),
    RendererInterface::class => factory(TwigRendererFactory::class),
    AutoRoute::class => autowire()->constructor('App\HTTP', \dirname(__DIR__) . '/src/HTTP/'),
];
